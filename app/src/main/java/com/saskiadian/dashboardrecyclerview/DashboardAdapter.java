package com.saskiadian.dashboardrecyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashBoardHolder>{

    private ArrayList<SetterGetter> listdata;

    public DashboardAdapter(ArrayList<SetterGetter> listdata) {
        this.listdata = listdata;
    }
    @NonNull
    @Override
    public DashBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view       = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent, false);
        DashBoardHolder holder = new DashBoardHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull DashBoardHolder holder, int position){

        final SetterGetter getData      =   listdata.get(position);
        String titlemenu        =   getData.getTitle();
        String logomenu         =   getData.getImg();

        holder.titleMenu.setText(titlemenu);
        if(logomenu.equals("logomenu1")){
            holder.imgMenu.setImageResource(R.drawable.informasi);
        }else if(logomenu.equals("logomenu2")){
            holder.imgMenu.setImageResource(R.drawable.kategori);
        }else if(logomenu.equals("logomenu3")){
            holder.imgMenu.setImageResource(R.drawable.keranjang);
        }else if(logomenu.equals("logomenu4")){
            holder.imgMenu.setImageResource(R.drawable.rekomendasi);
        }else if(logomenu.equals("logomenu5")){
            holder.imgMenu.setImageResource(R.drawable.favorit);
        }else if(logomenu.equals("logomenu6")){
            holder.imgMenu.setImageResource(R.drawable.pesanan);
        }else if(logomenu.equals("logomenu7")){
            holder.imgMenu.setImageResource(R.drawable.bantuan);
        }else if(logomenu.equals("logomenu8")){
            holder.imgMenu.setImageResource(R.drawable.rating);
        }else if(logomenu.equals("logomenu9")){
            holder.imgMenu.setImageResource(R.drawable.pengaturan);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashBoardHolder extends RecyclerView.ViewHolder {

        TextView titleMenu;
        ImageView imgMenu;

        public DashBoardHolder(@NonNull View itemView) {
            super(itemView);

            titleMenu       =   itemView.findViewById(R.id.tv_title_menu);
            imgMenu         =   itemView.findViewById(R.id.iv_logo_menu);
        }
    }

}
