package com.saskiadian.dashboardrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;
    GridLayoutManager gridLayoutManager;
    DashboardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_menu);

        adddata();
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashboardAdapter(datamenu);
        recyclerView.setAdapter(adapter);

    }
    public void adddata(){
        datamenu        =   new ArrayList<>();
        datamenu.add(new SetterGetter("Informasi Toko", "logomenu1"));
        datamenu.add(new SetterGetter("Kategori", "logomenu2"));
        datamenu.add(new SetterGetter("Keranjang Saya", "logomenu3"));
        datamenu.add(new SetterGetter("Rekomendasi", "logomenu4"));
        datamenu.add(new SetterGetter("Favorit", "logomenu5"));
        datamenu.add(new SetterGetter("Pesanan Saya", "logomenu6"));
        datamenu.add(new SetterGetter("Pusat Bantuan", "logomenu7"));
        datamenu.add(new SetterGetter("Rating", "logomenu8"));
        datamenu.add(new SetterGetter("Pengaturan", "logomenu9"));

    }
}